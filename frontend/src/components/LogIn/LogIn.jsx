import React, { useState } from 'react'
import './LogIn.css'
import { assets } from '../../assets/assets'

const LogIn = ({setShowLogin}) => {
    const [currState,setCurrState] = useState("Sign Up")
  return (
    <div className='login'>
      <form className="login-container">
        <div className="login-title">
            <h2>{currState}</h2>
            <img onClick={()=>setShowLogin(false)} src={assets.cross_icon} alt="" />
        </div>
        <div className="login-inputs">
            {currState==="LogIn"?<></>:<input type="text" placeholder='Name' required/>}
            <input type="text" placeholder='Email' required/>
            <input type="password" placeholder='Password' required/>
        </div>
        <button>{currState==="Sign Up"?"Create account":"LogIn"}</button>
        <div className="login-condition">
            <input type="checkbox" required />
            <p>Terms and conditions</p>
        </div>
        {currState==="LogIn"?<p>Create account <span onClick={()=>setCurrState("Sign Up")}> Click here</span></p>:<p>Already have an account? <span onClick={()=>setCurrState("LogIn")}>Log In here</span></p>}
      </form>
    </div>
  )
}

export default LogIn
